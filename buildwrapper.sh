#!/bin/bash

repo=$1
branch=$2
outputName=$3
token=$4
user=$5
outputFolder=$6
outputTag=$7

echo "*** Building tmp-build-image ***"
docker build --build-arg REPO=$repo --build-arg BRANCH=$branch --build-arg TOKEN=$token --build-arg USER=$user --build-arg OUTPUT_NAME=$outputName -t tmp-build-image --rm ~/sources/go_build/
echo "*** starting tmp-build-container ***"
docker run --name tmp-build-container -d tmp-build-image /bin/bash -c "sleep 10"
echo "*** coping $outputName ***"
docker cp tmp-build-container:/build-tmp/${repo}/${outputName} ${outputFolder}/.
echo "*** removing tmp-build-container ***"
docker rm -f tmp-build-container
echo "*** removing tmp-build-image ***"
docker rmi tmp-build-image
echo "*** building output container ***"
docker build -t ${outputName}:${outputTag} --build-arg INPUT_FILE=${outputName} ${outputFolder}
echo "*** finished! ***"
