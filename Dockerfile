FROM golang:1.8.1-alpine

ARG REPO
ARG BRANCH
ARG TOKEN
ARG USER
ARG OUTPUT_NAME

RUN apk update && \
    apk add bash && \
    apk add bash-completion && \
    apk add git && \
    apk add curl

RUN mkdir /build-tmp && \
    cd /build-tmp && \
    git clone -b ${BRANCH} --single-branch https://x-token-auth:${TOKEN}@bitbucket.org/${USER}/${REPO} 

RUN cd /build-tmp/${REPO} && \
    go build -o ${OUTPUT_NAME} && \
    tar -cvf ${OUTPUT_NAME}.tar.gz ${OUTPUT_NAME}




    





